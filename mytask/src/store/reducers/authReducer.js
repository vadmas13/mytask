const initState = {
    authError: null
}

const authReducer = (state = initState, action) => {
    switch(action.type){
        case 'LOGIN_ERROR':
            console.log("error login");
            return {...state, authError: 'Login failed'};
        case 'LOGIN_SUCCESS':
            console.log("success login");
            return {...state, authError: null}
        case 'SIGNOUT_SUCCESS':
            console.log("success SIGNOUT");
            return state;
        case 'SIGNUP_SUCCESS':
            console.log("success SIGNUP");
            return {
                ...state,
                authError: null
            } ;
        case 'SIGNUP_ERROR':
            console.log("ERROR SIGNUP");
            return {
                ...state,
                authError: action.err.message
            } ;
        default:
            return state;

    }

}

export default authReducer;