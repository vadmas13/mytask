const initState = {
    projects: [
        {id: 1, title: 'Hey guys, what we doing', content:'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
        {id: 2, title: 'Integer tincidunt', content:'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'},
        {id: 3, title: 'Aenean imperdiet', content:'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'}
    ]

}

const projectReducer = (state = initState, action) => {
    switch (action.type){
        case 'CREATE_PROJECT' :
            console.log("yes", action.project)
            return state;
        case 'CREATE_PROJECT_ERROR' :
            console.log("no", action.err)
            return state;
        default:
            return state;
    }

}

export default projectReducer;