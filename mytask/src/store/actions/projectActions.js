

export const createProject = (project) =>
    (dispatch, getState, { getFirebase, getFirestore }) =>{

        let db = getFirestore();
        let profile = getState().firebase.profile;
        let authorId = getState().firebase.auth.uid;

        db.collection('projects').add({
        ...project,
        authorFirstName: profile.firstName,
        authorLastName: profile.lastName,
        authorId: authorId,
        createdAt: new Date()
    }).then(() => {
        dispatch({type: 'CREATE_PROJECT', project})
    }).catch((err) => {
        dispatch({type: 'CREATE_PROJECT_ERROR', err})
    })

}