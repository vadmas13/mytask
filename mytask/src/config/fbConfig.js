
import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {

    apiKey: "AIzaSyAoY8gbazT3ByYNscZuR2Z1PbFcP4af7IY",
    authDomain: "mytasks-8ccde.firebaseapp.com",
    databaseURL: "https://mytasks-8ccde.firebaseio.com",
    projectId: "mytasks-8ccde",
    storageBucket: "mytasks-8ccde.appspot.com",
    messagingSenderId: "680027749552",
    appId: "1:680027749552:web:11740d7d8530b8cc29cf75"
};
// Initialize Firebase




firebase.initializeApp(firebaseConfig);

export default firebase;
