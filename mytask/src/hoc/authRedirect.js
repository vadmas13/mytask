import React, {Component} from 'react'
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";

let authStateToProps = (state) => {
    return{
        authorize: state.firebase.auth
    }
}

export const authRedirect = (path) => (Comp) => {
    class withAuthRedirect extends Component{

        render(){
            let {authorize} = this.props;
            if(!authorize.uid) return <Redirect to={path} />;
            return <Comp {...this.props} />
        }
    }

    return connect(authStateToProps)(withAuthRedirect)
}
