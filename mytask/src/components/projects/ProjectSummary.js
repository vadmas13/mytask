import React from 'react'
import moment from "moment";

const ProjectSummary = (props) =>{
    return(
        <div className="project-list section">
            <div className="card z-depth-0 project-summary">
                <div className="card-content grey-text text-darken-3">
                    <span className="card-title">{ props.title }</span>
                    <p>Posted By { props.authorFirstName } { props.authorLastName }</p>
                    <p className="grey-text">{ moment(props.createdAt.toDate()).calendar() }</p>
                </div>
            </div>
        </div>
    )
}

export default ProjectSummary;