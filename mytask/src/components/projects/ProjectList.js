import React from 'react'
import ProjectSummary from "./ProjectSummary";
import {Link} from "react-router-dom";

const ProjectList = (props) =>{
    return(
        <div className="project-list section">
            { props.projects && props.projects.map( p => <Link to={`/project/${p.id}`}><ProjectSummary {...p} key={p.id}/></Link>) }
        </div>
    )
}

export default ProjectList;